package tests;

import classes.Customer;
import classes.Movie;
import classes.Rental;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertFalse;


public class CustomerTest {

    private Customer customer;

    @Test
    public void testGetName() {
        customer = new Customer("Hans");
        Assertions.assertEquals(customer.getName(), "Hans");
    }

    @Test
    public void addRental() {
        customer = new Customer("Hans");
        Rental rentalToAdd = new Rental(new Movie("Hans", 10), 3);
        customer.addRental(rentalToAdd);
        assertFalse(customer.getRentals().isEmpty());
    }

}
