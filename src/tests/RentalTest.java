package tests;

import classes.Movie;
import classes.Rental;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class RentalTest {

    private Rental rental;
    private Movie movie;
    private final int daysRented = 3;

    @BeforeEach
    void setUp() {
        movie = new Movie("Movietitle", 10);
        rental = new Rental(movie, daysRented);
    }

    @Test
    void getDaysRented() {
        assertEquals(daysRented, rental.getDaysRented());
    }

    @Test
    void getMovie() {
        assertEquals(movie, rental.getMovie());
    }
}
