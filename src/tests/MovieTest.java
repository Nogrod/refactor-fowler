package tests;

import classes.Movie;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MovieTest {

    private Movie movie;


    @Test
    public void getPriceCode() {
        movie = new Movie("TestMovie", 10);
        assertEquals(10, movie.getPriceCode());
    }

    @Test
    public void getTitle() {
        movie = new Movie("TestMovie", 10);
        assertEquals("TestMovie", movie.getTitle());
    }

    @Test
    public void setPriceCode() {
        movie = new Movie("TestMovie", 10);
        movie.setPriceCode(10);
        assertEquals(10, movie.getPriceCode());
    }
}
